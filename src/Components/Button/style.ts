import styled from 'styled-components';

interface ButtonProps {
	bgcolor: string;
	theme: any;
	width: string;
	height: string;
	ptb: string;
	prl: string;
	borderRadius: number;
	borderSize: number;
	btnbg:string;
}


const mainButton  = styled.button`
    display:block;
    font-size:16px;
    padding:5px;
`;


export const ButtonStyle = styled(mainButton)`
    

`