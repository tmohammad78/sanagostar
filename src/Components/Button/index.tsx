import React from 'react';
import {ButtonStyle} from './style'


interface IButton {

}

export const Button = ({children, ...props}:any) => {
    return (
        <ButtonStyle {...props}>{children}</ButtonStyle>
    )
}