import React from 'react';
import { InputStyle } from './style';

export const Input = ({children}) => {
    return (
        <InputStyle > {children} </InputStyle>
    );
}
