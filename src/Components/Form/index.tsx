import React , {Fragment} from 'react';
import { Formik, Field  , FieldAttributes} from 'formik';
import { validation } from '../../Utils/validation';
import { Input } from '../../Components/Input';
import { Button } from '../../Components/Button'

const RegisterForm = () => {

    const InputForm = ({ field, className, meta, form: { touched, errors }, ...props }: FieldAttributes<any>) => {
		return (
			<Fragment>
				<div className='inputPack  clearfix'>
					<Input {...field} {...props} />
					{errors[field.name] && <div className='errorMessage'>{errors[field.name]}</div>}
				</div>
			</Fragment>
		);
    };
    
    return (
        <div>
            <Formik 
                initialValues={{fName:'',lName:'',phoneNumber:'',landingPhone:'', address:'',sex:''}}
                validationSchema={validation}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        setSubmitting(false);
                    }, 400);
                }}
            >  
    {({
         values,
         errors,
         touched,
         handleChange,
         handleBlur,
         handleSubmit,
         isSubmitting,  
         /* and other goodies */
       }) => (
        <form>
            <Field name='firstname' type='text' label='fName' component={InputForm} />
            <Field name='lastname' type='text' label='lName' component={InputForm} />
            <Field name='phoneNumber' type='text' label='phoneNumber' component={InputForm} />
            <Field name='landingPhone' type='text' label='landingPhone' component={InputForm} />
            <Field name='address' type='text' label='address' component={InputForm} />
            <Field name='sex' type='text' label='sex' component={InputForm} />
            <Button
						type='submit'
						// onClick={test}
						className='registerBtn'
						onClick={() => submitForm(values)}
						disabled={isSubmitting}
					>
                        <span>مرحله ی بعد</span>
					</Button>
        </form>
       )    

}
            </Formik>

        </div>

    )
}
export default RegisterForm;