import React from 'react';
import RegisterForm from "./Components/Form";
import {Route} from 'react-router-dom';
const App = () => {
    return (
        <div>
            <Route component={RegisterForm} />
        </div>
    )
}
export default App;